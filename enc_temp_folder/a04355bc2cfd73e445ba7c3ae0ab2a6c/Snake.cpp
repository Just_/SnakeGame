// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include <thread>
#include <chrono>

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MovementSpeed = 10.f;
	SnakeLifeReductionTime = 0.04f;
	SnakeLivesCount = 1;
	bIsDead = false;
	bFoodEaten = false;
	bCanHitWalls = true;
	bBonusThree = false;
	LastMoveDirection = EMovementDirection::DOWN;
	Score = 0;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(0.25);
	AddSnakeElement(5);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	if (SnakeLivesCount <= 0)
	{
		bIsDead = true;
	}
	if (bIsDead == true)
	{ 
		Destroy();
	}
}

void ASnake::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num()*ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MoveSpeed = ElementSize;
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MoveSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MoveSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MoveSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MoveSpeed;
		break;
	}

	SnakeElements[0]->ToggleCollision();


	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	//std::this_thread::sleep_for(std::chrono::milliseconds(250));
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnake::HitWallsTurnOn()
{
	bBonusThree = true;
}

