// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float MovementSpeed;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float SnakeLifeReductionTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float SnakeLivesCount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int32 Score;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bFoodEaten;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bCanHitWalls;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bBonusThree;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bIsDead;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION()
	void HitWallsTurnOn();
};
